/*
 * Import custom JS helpers
 */
import './utils/helpers/index.ts'

import {createApp} from 'vue'
import App from './App.vue'
// import router from './router'

import '@/assets/scss/main.scss'
const app = createApp(App)

/*
 * External libs
 */

/*
 * Install app utils
 */
import utils from './utils/index'

utils(app)


// app.use(router)


/*
 * Wait util all routes are properly loaded
 */
// router.isReady().then(() => {
//
//     /*
//      * append wrappers for modal windows
//      // * - notifications
//      * - general modals
//      */
//     // const notifications_wrapper = document.createElement("div");
//     // notifications_wrapper.style.position = "relative";
//     // notifications_wrapper.style.zIndex = '3000';
//     // notifications_wrapper.id = 'notifications-wrapper';
//
//     const modals_wrapper = document.createElement("div");
//     modals_wrapper.style.position = "relative";
//     modals_wrapper.style.zIndex = '2000';
//     modals_wrapper.id = 'modals';
//
//     // document.body.prepend(notifications_wrapper);
//     document.body.prepend(modals_wrapper);
//
//     /*
//      *  Mount vue instance to DOM
//      */
//
//
// })

app.mount('#app')
