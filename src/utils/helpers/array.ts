Array.prototype['deleteOnIndex'] = function (index: number) {
    this.splice(index, 1);
}

Array.prototype.move = function (from: number, to: number) {
    this.splice(to, 0, this.splice(from, 1)[0]);
}

Array.prototype.mapOrder = function (order: never[], key: string | number) {

    this.sort(function (a, b) {
        const A = a[key], B = b[key];
        return (order.indexOf(A) > order.indexOf(B)) ? 1 : -1
    })

    return this;
}