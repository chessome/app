import {colsContentType, markColor, markFieldType} from "@/types/board";

const cols_content: colsContentType = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

const defaultMarkField = (): markFieldType => ({
    field: {
        row: 0,
        col: ''
    },
    color: markColor.transparent
})

export {
    cols_content,
    defaultMarkField
}

