import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'

/*
 *
 */
const routes: Array<RouteRecordRaw> = []

// console.log('ss', import.meta.env.VITE_BASE_URL)

const router = createRouter({
    history: createWebHistory('/'),
    routes,
    scrollBehavior(to, from, savedPosition) {
        // always scroll to top
        return {top: 0}
    },
})

export default router
