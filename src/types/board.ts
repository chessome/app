type rowType = number
type colType = string

type colsContentType = [string, string, string, string, string, string, string, string]

enum playAsOption {
    white = 'WHITE',
    black = 'BLACK',
}

type answerType = {
    col: string,
    row: number
}

interface markFieldType {
    field: answerType,
    color: markColor
}

enum markColor {
    green = 'GREEN',
    red = 'RED',
    transparent = 'TRANSPARENT',
}

enum squareColors {
    white = 'bg-white',
    black = 'bg-black'
}


export {
    colsContentType,
    rowType,
    colType,
    playAsOption,
    answerType,
    squareColors,
    markFieldType,
    markColor
}