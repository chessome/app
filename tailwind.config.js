module.exports = {
    // purge: [],
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
    ],
    safelist: [
        'bg-black',
        'bg-white',
        'bg-red',
        'bg-green',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                white: '#fff',
                dark: {
                    900: '#000',
                    800: '#151f27',
                    700: '#1b2730',
                    600: '#2e3a44',
                },
                grey: {
                    800: '#c7d0d8',
                    700: '#eff3f6',
                },
                blue: {
                    500: '#163ecd'
                },
                teal: {
                    500: '#00a6a6',
                    400: '#78ffd7',
                    300: '#81ffdf',
                },
                red: '#ec635e',
                green: '#16a34a'
            },
            zIndex: {
                '55': '55',
                '60': '60',
                '100': '100',
                '120': '120',
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
